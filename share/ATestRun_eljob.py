#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
#inputFilePath = '/afs/cern.ch/work/h/heljarra/samples/mc16_13TeV'
inputFilePath = '/afs/cern.ch/work/h/heljarra/samples/inputs/mc15_13TeV/mc15_13TeV/'
#inputFilePath = '/tmp/heljarra/mc15_13TeV/'
ROOT.SH.ScanDir().filePattern( '*' ).scan( sh, inputFilePath )
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 100 )

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )

# To fill trees
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )

# ask SampleHandler to plot the histogram (Remove this lines to submit your job to the Grid)
#sh_hist = ROOT.SH.SampleHandler()
#sh_hist.load (submitDir + '/hist')
#hist = sh_hist.get ('AOD.05352803._000031.pool.root.1').readHist('h_jetPt')
#hist.Draw()


