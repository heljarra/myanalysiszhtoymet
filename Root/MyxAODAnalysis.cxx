#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <TSystem.h>
#include "xAODCore/AuxContainerBase.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETComposition.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "METInterface/IMETMaker.h"
#include "METUtilities/CutsMETMaker.h"
#include "METUtilities/METHelpers.h"
#include "xAODBase/IParticleHelpers.h"
#include <xAODCore/ShallowAuxContainer.h>
#include <xAODCore/ShallowCopy.h>
#include <PATInterfaces/CorrectionCode.h>
#include "FourMomUtils/xAODP4Helpers.h"
#include "IsolationSelection/IsolationWP.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "AssociationUtils/OverlapRemovalDefs.h"
#include "AssociationUtils/OverlapRemovalTool.h"
#include "AssociationUtils/ToolBox.h"
#include "AssociationUtils/OverlapRemovalInit.h"

const static double Zmass = 91.2e3;
using namespace ORUtils;

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator),
m_grl ("GoodRunsListSelectionTool/grl", this),
m_jetCleaning ("JetCleaningTool/JetCleaning", this),
m_JERTool ("JERTool", this),
m_muonCalibrationAndSmearingTool ("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool",this),
iso_tool ("CP::IsolationSelectionTool", this),
m_EgammaCalibrationAndSmearingTool("CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool", this),
//m_orTool("ORUtils::OverlapRemovalTool", this)
m_orTool("ORUtils::IOverlapRemovalTool/MyORTool",this)


//*********************************************************Constructor***********************************************************
{
    m_grl.declarePropertyFor (this, "grlTool");
    declareProperty( "METName",        m_outname   = "MET_Reference"     );
    // declareProperty( "METMapName",     m_mapname   = "METAssoc_AntiKt4EMTopo" );
    declareProperty( "DoTruthLeptons", m_doTruthLep = false              );
    declareProperty( "InputElectrons", m_eleColl   = "Electrons"         );
   // declareProperty( "IEgammaCalibrationAndSmearingTool", m_EgammaCalibrationAndSmearingTool);
    m_orTool.declarePropertyFor (this, "OverlapRemovalTool");
    declareProperty("SelectionLabel", m_selectionLabel="selected", "Input label for the OverlapRemovalTool"); 
    declareProperty("OverlapLabel", m_overlapLabel="overlaps", "Output label for the OverlapRemovalTool");
    declareProperty("BJetLabel", m_bJetLabel="isBJet", "Input label for b-tagged jets");

}
//***********************************************************Destructor**********************************************************

MyxAODAnalysis :: ~MyxAODAnalysis () {
    delete m_jetEta;
    delete m_jetPhi;
    delete m_jetPt;
    delete m_jetE;    
    delete m_ElEta;
    delete m_PosEta;
    delete m_ElPhi;
    delete m_PosPhi;
    delete m_ElPt;
    delete m_PosPt;    
    delete m_MuplusEta;
    delete m_MuminusEta;
    delete m_MuplusPhi;
    delete m_MuminusPhi;
    delete m_MuplusPt;
    delete m_MuminusPt;
    delete m_GammafirstEta;
    delete m_GammafirstPhi;
    delete m_GammafirstPt;  
    delete m_RefJet;
    delete m_RefGamma;
    delete m_RefEle;
    delete m_Muons;
    delete m_invMassEP;
    delete m_invMassMuMu;
    delete m_MTGammaMET;
    delete m_SoftClus;
    delete m_PVSoftTrk;
    delete m_FinalClus;
    delete m_Final;
}
//***********************************************************Initialize**********************************************************
StatusCode MyxAODAnalysis :: initialize ()
{
    ANA_MSG_INFO ("in initialize");
    //==============================GRL (only for data)==============================

    const char* GRLFilePath = "$ALRB_TutorialData/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
    const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
    std::vector<std::string> vecStringGRL;
    vecStringGRL.push_back(fullGRLFilePath);
    ANA_CHECK(m_grl.setProperty( "GoodRunsListVec", vecStringGRL));
    ANA_CHECK(m_grl.setProperty("PassThrough", false));
    ANA_CHECK(m_grl.initialize());
    //==============================Jet Calibration Tool=============================
    ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetCalibrationTool_handle, JetCalibrationTool));
    ANA_CHECK( m_JetCalibrationTool_handle.setProperty("JetCollection",jetType));
    ANA_CHECK( m_JetCalibrationTool_handle.setProperty("ConfigFile", "JES_MC15Prerecommendation_AFII_June2015.config"));
    ANA_CHECK( m_JetCalibrationTool_handle.setProperty("CalibSequence", "JetArea_Residual_Origin_EtaJES_GSC"));
    ANA_CHECK( m_JetCalibrationTool_handle.setProperty("IsData", false ));
    ANA_CHECK( m_JetCalibrationTool_handle.retrieve());
    //===============================Jet Cleaning Tool===============================
    ANA_CHECK (m_jetCleaning.setProperty( "CutLevel", "LooseBad"));
    ANA_CHECK (m_jetCleaning.setProperty("DoUgly", false));
    ANA_CHECK (m_jetCleaning.initialize());
    //=============================Jet Energy Resolution=============================
    ANA_CHECK(m_JERTool.initialize());
    //============================ Electron Selection Tool===========================
    m_MediumLH = new AsgElectronLikelihoodTool ("MediumLHElectron_Rel20p7");
    ANA_CHECK(m_MediumLH->setProperty("WorkingPoint", "MediumLHElectron_Rel20p7"));
    ANA_CHECK(m_MediumLH->initialize());
    //============================ Photon Selection Tool=============================
    m_photonTightIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonTightIsEMSelector" );
    m_photonTightIsEMSelector->setProperty("isEMMask","PhotonTight");
    m_photonTightIsEMSelector->setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf");
    if (!m_photonTightIsEMSelector->initialize().isSuccess()) {
        Fatal("MyFunction", "Failed to initialize PhotonTightIsEMSelector"); }
    //========================Muon Calibration & Smearing Tool=======================
    ANA_CHECK (m_muonCalibrationAndSmearingTool.initialize());
    //======================EGamma Calibration & Smearing Tool=======================
    ANA_CHECK (m_EgammaCalibrationAndSmearingTool.setProperty("ESModel","es2015PRE_res_improved"));
    ANA_CHECK (m_EgammaCalibrationAndSmearingTool.setProperty("decorrelationModel","FULL_v1"));
    ANA_CHECK (m_EgammaCalibrationAndSmearingTool.setProperty("randomRunNumber", 123456));
    ANA_CHECK (m_EgammaCalibrationAndSmearingTool.setProperty("useAFII", 1));
    ANA_CHECK (m_EgammaCalibrationAndSmearingTool.initialize());
    //==================================Isolation====================================
    CP::IsolationSelectionTool iso_tool( "iso_tool" );
    ANA_CHECK(iso_tool.setProperty("MuonWP","Gradient") );
    ANA_CHECK(iso_tool.setProperty("ElectronWP","Gradient") );
    ANA_CHECK(iso_tool.setProperty("PhotonWP","Cone40") );
    ANA_CHECK(iso_tool.initialize() );
    //=================================METMaker Tools================================
    m_metmaker.setTypeAndName("met::METMaker/m_metmaker");
    ANA_CHECK( m_metmaker.setProperty("DoMuonEloss", false) );
    ANA_CHECK( m_metmaker.setProperty("DoRemoveMuonJets", true) );
    ANA_CHECK( m_metmaker.setProperty("DoSetMuonJetEMScale", true) );
    ANA_CHECK( m_metmaker.retrieve() );
    //=====================================Random====================================
    std::srand(1111111111);
    //================================= Overlap Removal==============================
    //ORUtils::IOverlapRemovalTool m_orTool( "m_orTool" );
    ORUtils::ORFlags orFlags("ORUtils::OverlapRemovalTool", inputLabel, outputLabel);
    ORUtils::ToolBox toolBox;
    ANA_CHECK( ORUtils::recommendedTools(orFlags, toolBox));
    ANA_CHECK( toolBox.muJetORT.setProperty("UseGhostAssociation", false) );
    ANA_CHECK( toolBox.muJetORT.setProperty("InnerDR", 0.) );
    ANA_CHECK( toolBox.phoEleORT.setProperty("SwapContainerPrecedence", true) );
    ANA_CHECK( toolBox.phoMuORT.setProperty("SwapContainerPrecedence", true) );
    ANA_CHECK(toolBox.initialize() ); 
    //ANA_CHECK(m_orTool.setProperty("toolBox","masterTool"));
     m_orTool = toolBox.masterTool;
    ANA_CHECK( m_orTool.retrieve() );
    //==============================Create Output Trees==============================
    ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
    TTree* mytree = tree ("analysis");
    mytree->Branch ("RunNumber", &m_runNumber);
    mytree->Branch ("EventNumber", &m_eventNumber);
    mytree->Branch ("MET_Truth_met", &m_truthmet);
    mytree->Branch ("MET_Truth_sumet", &m_truthsumet);
    m_jetEta = new std::vector<float>();
    mytree->Branch ("Jet_Eta", &m_jetEta);
    m_jetPhi = new std::vector<float>();
    mytree->Branch ("Jet_Phi", &m_jetPhi);
    m_jetPt = new std::vector<float>();
    mytree->Branch ("Jet_Pt", &m_jetPt);
    m_jetE = new std::vector<float>();
    mytree->Branch ("Jet_E", &m_jetE);
    m_ElEta = new std::vector<float>();
    mytree->Branch ("Electron1_Eta", &m_ElEta);
    m_PosEta = new std::vector<float>();
    mytree->Branch ("Electron2_Eta", &m_PosEta);
    m_ElPhi = new std::vector<float>();
    mytree->Branch ("Electron1_Phi", &m_ElPhi);
    m_PosPhi = new std::vector<float>();
    mytree->Branch ("Electron2_Phi", &m_PosPhi);      
    m_ElPt = new std::vector<float>();
    mytree->Branch ("Electron1_Pt", &m_ElPt);
    m_PosPt = new std::vector<float>();
    mytree->Branch ("Electron2_Pt", &m_PosPt);
    m_MuplusEta = new std::vector<float>();
    mytree->Branch ("Muon1_Eta", &m_MuplusEta);
    m_MuminusEta = new std::vector<float>();
    mytree->Branch ("Muon2_Eta", &m_MuminusEta);
    m_MuplusPhi = new std::vector<float>();
    mytree->Branch ("Muon1_Phi", &m_MuplusPhi);
    m_MuminusPhi = new std::vector<float>();
    mytree->Branch ("Muon2_Phi", &m_MuminusPhi);
    m_MuplusPt = new std::vector<float>();
    mytree->Branch ("Muon1_Pt", &m_MuplusPt);
    m_MuminusPt = new std::vector<float>();
    mytree->Branch ("Muon2_Pt", &m_MuminusPt);
    m_GammafirstEta = new std::vector<float>();
    mytree->Branch ("Photon1_Eta", &m_GammafirstEta);
    m_GammafirstPhi = new std::vector<float>();
    mytree->Branch ("Photon1_Phi", &m_GammafirstPhi);
    m_GammafirstPt = new std::vector<float>();
    mytree->Branch ("Photon1_Pt", &m_GammafirstPt);
    m_RefJet = new std::vector<float>();
    mytree->Branch ("JetMET_RefJet", &m_RefJet);
    m_RefGamma = new std::vector<float>();
    mytree->Branch ("PhotonMET_RefGamma", &m_RefGamma);
    m_RefEle = new std::vector<float>();
    mytree->Branch ("ElectronMET_RefEle", &m_RefEle);
    m_Muons = new std::vector<float>();
    mytree->Branch ("MuonMET_Muons", &m_Muons);
    m_invMassEP = new std::vector<float>();
    mytree->Branch ("Invariante_Mass_Electron1Electron2", &m_invMassEP);
    m_invMassMuMu = new std::vector<float>();
    mytree->Branch ("Invariante_Mass_Muon1Muon2", &m_invMassMuMu);
    m_MTGammaMET = new std::vector<float>();
    mytree->Branch ("Transverse_Mass_Gamma_MET", &m_MTGammaMET);
    m_SoftClus = new std::vector<float>();
    mytree->Branch ("SoftCluster_MET", &m_SoftClus);
    m_PVSoftTrk = new std::vector<float>();
    mytree->Branch ("PVSoftTrack_MET", &m_PVSoftTrk);
    m_FinalClus = new std::vector<float>();
    mytree->Branch ("FinalClus_MET", &m_FinalClus); 
    m_Final = new std::vector<float>();
    mytree->Branch ("Final_MET", &m_Final);
    return StatusCode::SUCCESS;
}
//************************************************************Execute************************************************************
StatusCode MyxAODAnalysis :: execute ()
{   //   ANA_MSG_INFO ("in execute");
    //===============Retrieve the eventInfo object from the event store==============
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
    //============================is the event data or MC============================
    bool isMC = false;
    if (eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
        isMC = true;
    }
    if (!isMC) {
        if (!m_grl->passRunLB(*eventInfo)) {
            ANA_MSG_INFO ("drop event: GRL");
            return StatusCode::SUCCESS;
        }
    }
    //===Event Cleaning only for data to remove events due to problematic regions====
    //======================of the detector or incomplete events=====================
    if (!isMC) {
        if ((eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) ||
            (eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ||
            (eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) ||
            (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18))) {
            return StatusCode::SUCCESS;
        }
    }
    m_numCleanEvents++;
    //===============================================================================
    //                        Access to Physics Objects
    //===============================================================================
    //@Start by MET to rebuild directly the met at each step after applying the cuts@
    //@&@&@&@&@&@&@&@&@&@& Retrieve the original MET container @&@&@&@&@&@&@&@&@&@&@&
    const xAOD::MissingETContainer* coreMet  = nullptr;
    std::string coreMetKey = "MET_Core_" + jetType;
    ANA_CHECK( evtStore()->retrieve(coreMet, coreMetKey) );
    //@&@&@&@&@&@&@&@&@&@&@& Retrieve the MET association map @&@&@&@&@&@&@&@&@&@&@&@
    const xAOD::MissingETAssociationMap* metMap = nullptr;
    std::string metAssocKey = "METAssoc_" + jetType;
    ANA_CHECK( evtStore()->retrieve(metMap, metAssocKey) );
    metMap->resetObjSelectionFlags();
    //@&@&@&@&@&@&@&@& Create a MissingETContainer with its aux store @&@&@&@&@&@&@&@
    xAOD::MissingETContainer*    newMetContainer    = new xAOD::MissingETContainer();
    xAOD::MissingETAuxContainer* newMetAuxContainer = new xAOD::MissingETAuxContainer();
    newMetContainer->setStore(newMetAuxContainer);
    //@&@&@&@&@& Reset the selected objects before every MET calculation &@&@&@&@&@&@
    metMap->resetObjSelectionFlags();
    //==========================Retrieve the original containers=====================
    //================================= Object selection ============================
    //@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@& Jets &@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&
    int nJets(0);
    const xAOD::JetContainer* jets = nullptr;
    ANA_CHECK( evtStore()->retrieve(jets, jetType+"Jets"));
    std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets );
    ANA_CHECK(evtStore()->record( jets_shallowCopy.first,  "CalibJets"    ));
    ANA_CHECK(evtStore()->record( jets_shallowCopy.second, "CalibJetsAux."));
    xAOD::JetContainer* calibJets = jets_shallowCopy.first;
    xAOD::setOriginalObjectLink(*jets,*calibJets);
    for ( const auto& jet : *calibJets ) {
        if(!m_JetCalibrationTool_handle->applyCalibration(*jet))continue;
        if (!m_jetCleaning->keep (*jet))continue;
        if(jet->pt()* 0.001 > 20) nJets++;
    }
     //  if(nJets > 0) return 1;
    //@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@& Photons &@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@
    int nPhotons(0);
    const xAOD::PhotonContainer* photons = nullptr;
    ANA_CHECK( evtStore()->retrieve(photons, "Photons"));
    std::pair< xAOD::PhotonContainer*, xAOD::ShallowAuxContainer* > photons_shallowCopy = xAOD::shallowCopyContainer( *photons );
    ANA_CHECK(evtStore()->record( photons_shallowCopy.first,  "CalibPhotons"    ));
    ANA_CHECK(evtStore()->record( photons_shallowCopy.second, "CalibPhotonsAux."));
    xAOD::PhotonContainer* calibPhotons = photons_shallowCopy.first;
    xAOD::setOriginalObjectLink(*photons,*calibPhotons);
    ConstDataVector<xAOD::PhotonContainer> goodPhotons(SG::VIEW_ELEMENTS);
    for(const auto& ph : *calibPhotons) {
    ANA_CHECK(m_EgammaCalibrationAndSmearingTool->applyCorrection(*ph));
    if(ph->pt()* 0.001 >25 && fabs(ph->eta())<2.37 && (fabs(ph->eta())<1.37 || fabs(ph->eta())>1.52) && m_photonTightIsEMSelector->accept(ph) && iso_tool->accept(*ph)) { goodPhotons.push_back(ph);
              nPhotons++; } }
    //@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@ Electrons &@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&
    int nElectrons(0);
    const xAOD::ElectronContainer* electrons = nullptr;
    ANA_CHECK( evtStore()->retrieve(electrons, "Electrons") );
    std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > electrons_shallowCopy = xAOD::shallowCopyContainer( *electrons );
    ANA_CHECK(evtStore()->record( electrons_shallowCopy.first,  "CalibElectrons"    ));
    ANA_CHECK(evtStore()->record( electrons_shallowCopy.second, "CalibElectronsAux."));
    xAOD::ElectronContainer* calibElectrons = electrons_shallowCopy.first;
    xAOD::setOriginalObjectLink(*electrons,*calibElectrons);
    ConstDataVector<xAOD::ElectronContainer> goodElectrons(SG::VIEW_ELEMENTS);
    for(const auto& el : *calibElectrons) {
    ANA_CHECK(m_EgammaCalibrationAndSmearingTool->applyCorrection(*el));
    if(el->pt()* 0.001 >25 && fabs(el->eta())<2.47 && (fabs(el->eta())<1.37 || fabs(el->eta())>1.52) && m_MediumLH->accept(el) && iso_tool->accept(*el) )
        {  goodElectrons.push_back(el);
                nElectrons++; } }
    //@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@& Muons &@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@
    int nMuons(0);
    const xAOD::MuonContainer* muons = nullptr;
    ANA_CHECK( evtStore()->retrieve(muons, "Muons") );
    std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
    ANA_CHECK(evtStore()->record( muons_shallowCopy.first,  "CalibMuons"    ));
    ANA_CHECK(evtStore()->record( muons_shallowCopy.second, "CalibMuonsAux."));
    xAOD::MuonContainer* calibMuons = muons_shallowCopy.first;
    xAOD::setOriginalObjectLink(*muons,*calibMuons);
    ConstDataVector<xAOD::MuonContainer> goodMuons(SG::VIEW_ELEMENTS);
    for(const auto& mu : *calibMuons) {
    ANA_CHECK(m_muonCalibrationAndSmearingTool->applyCorrection(*mu));
    if(mu->muonType()==xAOD::Muon::Combined && mu->pt()* 0.001 >25 && fabs(mu->eta())<2.7 && iso_tool->accept(*mu) && (*mu).quality() <= xAOD::Muon::Medium ) {  goodMuons.push_back(mu);
            nMuons++; } }

    //==============================Apply Overlap Removal============================

ANA_CHECK( m_orTool->removeOverlaps(electrons, muons, jets, nullptr, photons, nullptr) );

    //@&@&@&@&@&@&@&@&@&@&@& Event selection ZH-> l+ l- + G G &@&@&@&@&@&@&@&@&@&@&@&
    bool ZmumuGG = false;
    bool ZeeGG = false;
    double MuinvMass=0,ElinvMass=0;
    TLorentzVector  Mup, Mum;
    if (nMuons==2) {
        Mup.SetPtEtaPhiE(goodMuons.at(0)->pt()*0.001 ,goodMuons.at(0)->eta(),goodMuons.at(0)->phi(),goodMuons.at(0)->e()*0.001);
        Mum.SetPtEtaPhiE(goodMuons.at(1)->pt()*0.001,goodMuons.at(1)->eta(),goodMuons.at(1)->phi(),goodMuons.at(1)->e()*0.001);
        double MupxSum = (Mup.Px() + Mum.Px());
        double MupySum = (Mup.Py() + Mum.Py());
        double MupzSum = (Mup.Pz() + Mum.Pz());
        double MueSum = (Mup.E() + Mum.E());
        MuinvMass = sqrt(MueSum*MueSum - MupxSum*MupxSum - MupySum*MupySum - MupzSum*MupzSum);
        if (nPhotons==2 && MuinvMass > 80. && MuinvMass < 110. && goodMuons.at(0)->charge() * goodMuons.at(1)->charge() < 0) { ZmumuGG = true;  } }
    TLorentzVector  el, po;
    if(nElectrons==2) {
        el.SetPtEtaPhiE(goodElectrons.at(0)->pt()*0.001,goodElectrons.at(0)->eta(),goodElectrons.at(0)->phi(),goodElectrons.at(0)->e()*0.001);
        po.SetPtEtaPhiE(goodElectrons.at(1)->pt()*0.001,goodElectrons.at(1)->eta(),goodElectrons.at(1)->phi(),goodElectrons.at(1)->e()*0.001);
        double ElpxSum = (el.Px() + po.Px());
        double ElpySum = (el.Py() + po.Py());
        double ElpzSum = (el.Pz() + po.Pz());
        double EleSum = (el.E() + po.E());
        ElinvMass = sqrt(EleSum*EleSum - ElpxSum*ElpxSum - ElpySum*ElpySum - ElpzSum*ElpzSum);
        if (nPhotons==2 && ElinvMass > 80. && ElinvMass < 110. && goodElectrons.at(0)->charge() * goodElectrons.at(1)->charge() < 0 ){ ZeeGG = true; } }

    if (!ZmumuGG && !ZeeGG) return 1;    
    //std::cout << "nPh, nEl, nMu " << nPhotons << "," << nElectrons << "," << nMuons << std::endl;;
    //@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&@ Mark Invisible  &@&@&@&@&@&@&@&@&@&@&@&@&@&@&@&
    ConstDataVector<xAOD::PhotonContainer> Photon_1(SG::VIEW_ELEMENTS); Photon_1.push_back(goodPhotons.at(0));    
    ConstDataVector<xAOD::PhotonContainer> Photon_2(SG::VIEW_ELEMENTS); Photon_2.push_back(goodPhotons.at(1));   
    //@&@&@&@&@&@&@&@&@&@&@&@&@& Rebuild Hard Objects Met  &@&@&@&@&@&@&@&@&@&@&@&@&@

    double ptvisibleG = 0;
    double phivisibleG = 0;    
    if (rand() < 0.5) { 
        ANA_CHECK(m_metmaker->markInvisible( Photon_1.asDataVector(),metMap,newMetContainer) );     
        ANA_CHECK(m_metmaker->rebuildMET("RefGamma",xAOD::Type::Photon,newMetContainer,Photon_2.asDataVector(),metMap));
        ptvisibleG = Photon_2.at(0)->pt()*0.001;
        phivisibleG = Photon_2.at(0)->phi(); }
    else {
        ANA_CHECK(m_metmaker->markInvisible( Photon_2.asDataVector(),metMap,newMetContainer) );
        ANA_CHECK(m_metmaker->rebuildMET("RefGamma",xAOD::Type::Photon,newMetContainer,Photon_1.asDataVector(),metMap));
        ptvisibleG = Photon_1.at(0)->pt()*0.001;
        phivisibleG = Photon_1.at(0)->phi(); } 
    ANA_CHECK(m_metmaker->rebuildMET("RefEle",xAOD::Type::Electron,newMetContainer, goodElectrons.asDataVector(),metMap));
    ANA_CHECK(m_metmaker->rebuildMET("Muons",xAOD::Type::Muon,newMetContainer,goodMuons.asDataVector(), metMap));
    met::addGhostMuonsToJets(*muons, *calibJets);
    //@&@&@&@&@&@&@&@&@&@& Rebuild jetMet and get the soft term &@&@&@&@&@&@&@&@&@&@&
    //&@  Create an xAODMissingET object with the given names inside the container @&
    ANA_CHECK( m_metmaker->rebuildJetMET("RefJet","SoftClus","PVSoftTrk", newMetContainer,calibJets, coreMet, metMap,true ));
    //====Build the final and cluster met sums, using systematic varied container====
    ANA_CHECK( m_metmaker->buildMETSum("FinalClus", newMetContainer, MissingETBase::Source::LCTopo) );
    ANA_CHECK( m_metmaker->buildMETSum("Final", newMetContainer, MissingETBase::Source::Track) );
    for(const auto& met : *newMetContainer){
        if(MissingETBase::Source::isTotalTerm(met->source())){ //std::cout << "met: " << met->met() << std::endl;; 
       } }
    ANA_CHECK(evtStore()->record( newMetContainer,    "FinalMETContainer"    ));
    ANA_CHECK(evtStore()->record( newMetAuxContainer, "FinalMETContainerAux."));
    //================================Fill the Trees=================================
        m_jetEta->clear();
        m_jetPhi->clear();
        m_jetPt->clear();
        m_jetE->clear();
        m_ElEta->clear();
        m_PosEta->clear();
        m_ElPhi->clear();
        m_PosPhi->clear();
        m_ElPt->clear();
        m_PosPt->clear();
        m_invMassEP->clear();
        m_MuplusEta->clear();
        m_MuminusEta->clear();
        m_MuplusPhi->clear();
        m_MuminusPhi->clear();
        m_MuplusPt->clear();
        m_MuminusPt->clear();
        m_invMassMuMu->clear();
        m_GammafirstEta->clear();
        m_GammafirstPhi->clear();
        m_GammafirstPt->clear();
        m_MTGammaMET->clear();
        m_RefJet->clear();
        m_RefGamma->clear();
        m_RefEle->clear();
        m_Muons->clear();
        m_SoftClus->clear();
        m_PVSoftTrk->clear();
        m_FinalClus->clear();
        m_Final->clear();

    const xAOD::EventInfo* ei = nullptr;
    ANA_CHECK (evtStore()->retrieve (ei, "EventInfo"));
    m_runNumber = ei->runNumber();
    m_eventNumber = ei->eventNumber();

    const xAOD::MissingETContainer* mettruth = nullptr;
    ANA_CHECK (evtStore()->retrieve (mettruth, "MET_Truth"));
    for (const xAOD::MissingET* truth : *mettruth) {
        m_truthmet ->push_back (truth->met() * 0.001);
        m_truthsumet ->push_back (truth->sumet() * 0.001); }

    for (const xAOD::Jet* jet : *jets) {
        m_jetEta->push_back (jet->eta ());
        m_jetPhi->push_back (jet->phi ());
        m_jetPt-> push_back (jet->pt () * 0.001);
        m_jetE->  push_back (jet->e () * 0.001); }

    if (ZeeGG) { //std::cout << "ZeenPh, ZeenEl, nMu " << nPhotons << "," << nElectrons << "," << nMuons << std::endl;;
        m_ElEta->push_back (goodElectrons.at(0)->eta());
        m_ElPhi->push_back (goodElectrons.at(0)->phi());
        m_ElPt-> push_back (goodElectrons.at(0)->pt() * 0.001);
        m_PosEta->push_back (goodElectrons.at(1)->eta());
        m_PosPhi->push_back (goodElectrons.at(1)->phi());
        m_PosPt-> push_back (goodElectrons.at(1)->pt() * 0.001);
        m_invMassEP-> push_back (ElinvMass); }

    if(ZmumuGG) { //std::cout << "ZmumunPh, nEl, nMu " << nPhotons << "," << nElectrons << "," << nMuons << std::endl;;
        m_MuplusEta->push_back (goodMuons.at(0)->eta ());
        m_MuplusPhi->push_back (goodMuons.at(0)->phi ());
        m_MuplusPt-> push_back (goodMuons.at(0)->pt () * 0.001);
        m_MuminusEta->push_back (goodMuons.at(1)->eta ());
        m_MuminusPhi->push_back (goodMuons.at(1)->phi ());
        m_MuminusPt-> push_back (goodMuons.at(1)->pt () * 0.001);
        m_invMassMuMu-> push_back (MuinvMass); }

    m_GammafirstEta->push_back (goodPhotons.at(0)->eta ());
    m_GammafirstPhi->push_back (goodPhotons.at(0)->phi ());
    m_GammafirstPt-> push_back (goodPhotons.at(0)->pt ()* 0.001);
    double Metphi = static_cast<xAOD::MissingET*>(*(newMetContainer->find("Final")))->phi();
    double MetFinal = static_cast<xAOD::MissingET*>(*(newMetContainer->find("Final")))->met() * 0.001;
    double DeltaPhi = xAOD::P4Helpers::deltaPhi(phivisibleG,Metphi);
    double GmMT = sqrt(2 * ptvisibleG * MetFinal * (1 - DeltaPhi) );
    m_MTGammaMET-> push_back (GmMT); 

    m_RefJet-> push_back(static_cast<xAOD::MissingET*>(*(newMetContainer->find("RefJet")))->met() * 0.001);
    m_RefGamma-> push_back(static_cast<xAOD::MissingET*>(*(newMetContainer->find("RefGamma")))->met() * 0.001);
    m_RefEle-> push_back(static_cast<xAOD::MissingET*>(*(newMetContainer->find("RefEle")))->met() * 0.001);
    m_Muons-> push_back(static_cast<xAOD::MissingET*>(*(newMetContainer->find("Muons")))->met() * 0.001);
    m_SoftClus-> push_back(static_cast<xAOD::MissingET*>(*(newMetContainer->find("SoftClus")))->met() * 0.001);
    m_PVSoftTrk-> push_back(static_cast<xAOD::MissingET*>(*(newMetContainer->find("PVSoftTrk")))->met() * 0.001);
    m_FinalClus-> push_back(static_cast<xAOD::MissingET*>(*(newMetContainer->find("FinalClus")))->met() * 0.001);
    m_Final-> push_back(static_cast<xAOD::MissingET*>(*(newMetContainer->find("Final")))->met() * 0.001);

    //========================= Fill the event into the tree =======================
    tree ("analysis")->Fill ();

    return StatusCode::SUCCESS;
}
//************************************************************Finalize**********************************************************
StatusCode MyxAODAnalysis :: finalize ()
{
    ANA_MSG_INFO ("in finalize");
    ANA_MSG_INFO ("finalize(): Number of clean events = " << m_numCleanEvents);   
    return StatusCode::SUCCESS;
}



