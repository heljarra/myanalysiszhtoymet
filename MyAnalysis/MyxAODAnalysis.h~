#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>
#include <TH1.h>
#include <TTree.h>
#include <vector>
#include <JetInterface/IJetSelector.h>
#include <JetResolution/IJERTool.h>
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>

#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODMissingET/MissingETContainer.h"

#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "IsolationSelection/IsolationWP.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "ElectronPhotonFourMomentumCorrection/IEgammaCalibrationAndSmearingTool.h"
class IMETMaker;

class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
   ~MyxAODAnalysis () override;

private:
  // Configuration, and any other types of variables go here.
int m_numCleanEvents = 0;
// Tools 
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl;
  asg::AnaToolHandle<IJetSelector> m_jetCleaning;  
  AsgElectronLikelihoodTool* m_MediumLH ;
  AsgPhotonIsEMSelector* m_photonTightIsEMSelector;
  // JER
  asg::AnaToolHandle<IJERTool> m_JERTool; 
  // MuonSelectionTool
  //asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelection; 
  asg::AnaToolHandle<CP::IsolationSelectionTool> iso_tool; 
  asg::AnaToolHandle<CP::MuonSelectionTool> m_muonSelection;
  // MuonCalibrationAndSmearing
  asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool; 
  std::string jetType = "AntiKt4EMTopo";
  asg::AnaToolHandle<IMETMaker> m_metmaker;
  asg::AnaToolHandle<IJetCalibrationTool> m_JetCalibrationTool_handle;
  asg::AnaToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_EgammaCalibrationAndSmearingTool;

 // asg::AnaToolHandle<IJetCalibrationTool>        m_JetCalibrationTool_handle{"JetCalibrationTool"};    
  //asg::AnaToolHandle<IJetCalibrationTool>        m_JetCalibrationTool_handle{"JetCalibrationTool"};         //!
  //asg::AnaToolHandle<IJetSelector>               m_JetCleaningTool_handle{"JetCleaningTool"};               //!
 // asg::AnaToolHandle<IJetCalibrationTool> jetCalibrationTool;
  std::string m_outname;
 //std::string m_mapname;
 bool m_doTruthLep;
// bool debug(false);
 bool accept(const xAOD::Electron* el);

 std::string m_eleColl;
 
/// output variables for the current event
  /// \{
  unsigned int m_runNumber = 0; ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number

  /// Jet 4-momentum variables
  std::vector<float> *m_truthmet = nullptr;
  std::vector<float> *m_truthsumet = nullptr;
  std::vector<float> *m_jetEta = nullptr;
  std::vector<float> *m_jetPhi = nullptr;
  std::vector<float> *m_jetPt = nullptr;
  std::vector<float> *m_jetE = nullptr;

  std::vector<float> *m_ElEta = nullptr;
  std::vector<float> *m_PosEta = nullptr;
  std::vector<float> *m_ElPhi = nullptr;
  std::vector<float> *m_PosPhi = nullptr;
  std::vector<float> *m_ElPt = nullptr;
  std::vector<float> *m_PosPt = nullptr;

  std::vector<float> *m_MuplusEta = nullptr;
  std::vector<float> *m_MuminusEta = nullptr;
  std::vector<float> *m_MuplusPhi = nullptr;
  std::vector<float> *m_MuminusPhi = nullptr;
  std::vector<float> *m_MuplusPt = nullptr;
  std::vector<float> *m_MuminusPt = nullptr;

  std::vector<float> *m_GammafirstEta = nullptr;
  std::vector<float> *m_GammasecondEta = nullptr;
  std::vector<float> *m_GammafirstPhi = nullptr;
  std::vector<float> *m_GammasecondPhi = nullptr;
  std::vector<float> *m_GammafirstPt = nullptr;
  std::vector<float> *m_GammasecondPt = nullptr;

  std::vector<float> *m_RefJet = nullptr;
  std::vector<float> *m_RefGamma = nullptr;
  std::vector<float> *m_RefEle = nullptr;
  std::vector<float> *m_Muons = nullptr;
  std::vector<float> *m_invMassEP = nullptr;
  std::vector<float> *m_invMassMuMu = nullptr;
  std::vector<float> *m_MTGammaMET = nullptr;
  std::vector<float> *m_invMassGG = nullptr;
  std::vector<float> *m_SoftClus = nullptr;
  std::vector<float> *m_PVSoftTrk = nullptr;
  std::vector<float> *m_FinalClus = nullptr;
  std::vector<float> *m_Final = nullptr;




  /// \}
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;
};

#endif
